FROM mhart/alpine-node:6

# Create app directory
RUN mkdir -p /usr/local/var/drone/test
WORKDIR /usr/local/var/drone/test

# Install app dependencies
#COPY package.json /usr/local/var/teal-node/backend
#RUN ./dashborad npm install

# Bundle app source
COPY . /usr/local/var/drone/test

#EXPOSE 3000
#CMD [ "npm", "start" ]
